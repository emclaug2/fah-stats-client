import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TeamStats} from '../../shared/model/TeamStats';
import {TeamDto} from '../../shared/dto/TeamDto';
import {Pair} from '../../shared/dto/Pair';
import {FahDataFormatService} from '../fah-data-format/fah-data-format.service';

@Injectable({
  providedIn: 'root'
})
export class FahApiService {

  serviceUrl = 'https://api-beta-dot-fah-stats-api.appspot.com/';

  constructor(private http: HttpClient,
              private dataFormatService: FahDataFormatService) { }

  getTop100Pairs(): Promise<Pair[]> {
    return new Promise((resolve, reject) => {
      this.trackTop100Search();
      this.http.get(this.serviceUrl + 'top100').toPromise()
        .then(resolve).catch(reject);
    });
  }

  getTeamStats(ids: number[]): Promise<TeamStats[]> {
    return new Promise((resolve, reject) => {
      this.trackTeamsSearch(ids);
      this.http.post(this.serviceUrl + 'teamStats', { ids }).toPromise()
        .then((teams: TeamDto[]) => {
          resolve(this.dataFormatService.formatFahData(teams));
        }).catch(reject);
    });
  }

  trackTeamsSearch = (ids: number[]) => {
    for (const id of ids) {
      (<any>window).ga('send', 'event', {
        eventCategory: '/teamStats',
        eventLabel: id,
        eventAction: '/teamStats',
        eventValue: id
      });
    }
  }

  trackTop100Search = () => {
    (<any>window).ga('send', 'event', {
      eventCategory: '/top100',
      eventLabel: '/top100',
      eventAction: '/top100',
      eventValue: '/top100'
    });
  }
}
