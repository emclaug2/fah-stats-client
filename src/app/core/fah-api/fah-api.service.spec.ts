import { TestBed } from '@angular/core/testing';

import { FahApiService } from './fah-api.service';

describe('FahApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FahApiService = TestBed.get(FahApiService);
    expect(service).toBeTruthy();
  });
});
