import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FahApiService} from './fah-api/fah-api.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {FahDataFormatService} from './fah-data-format/fah-data-format.service';
@NgModule({
  declarations: [],
  imports: [
    HttpClientModule,
    CommonModule
  ],
  providers: [
    {
      provide: FahApiService,
      useClass: FahApiService,
      deps: [HttpClient, FahDataFormatService]
    }
  ]
})
export class CoreModule { }
