import {Injectable} from '@angular/core';
import {TeamStats} from '../../shared/model/TeamStats';
import {TeamDto} from '../../shared/dto/TeamDto';
import {Datapoint} from '../../shared/model/Datapoint';

@Injectable({
  providedIn: 'root'
})
export class FahDataFormatService {

  // Pads missing data, creates map from DTO.
  formatFahData(teams: TeamDto[]): TeamStats[] {
    const teamStats = [];
    const timestamps = this.getAllTimestamps(teams);

    for (const team of teams) {
      const map = new Map<number, Datapoint>();
      for (const timestamp of timestamps) {
        const credit = team.date.hasOwnProperty(timestamp) ? team.date[timestamp].credit : undefined;
        map.set(Number(timestamp), new Datapoint(credit));
      }
      teamStats.push(new TeamStats(team.id, team.name, map));
    }
    return teamStats;
  }

  // Goes through all the teams and finds all the timestamps which we need data for.
  getAllTimestamps(teams: TeamDto[]): string[] {
    const timestampSet = new Set<string>();
    for (const team of teams) {
      if (team.date instanceof Map) {
        for (const timestamp of Array.from(team.date.keys())) {
          timestampSet.add(timestamp);
        }
      } else {
        for (const timestamp in team.date) {
          timestampSet.add(timestamp);
        }
      }
    }
    return Array.from(timestampSet.values()).sort();
  }
}
