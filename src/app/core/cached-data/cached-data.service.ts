import { Injectable } from '@angular/core';
import {Pair} from '../../shared/dto/Pair';
import {TeamStats} from '../../shared/model/TeamStats';

@Injectable({
  providedIn: 'root'
})
export class CachedDataService {

  private data: Map<number, TeamStats> = new Map<number, TeamStats>();
  teamNames: Pair[] = [];

  cacheTeamData(stats: TeamStats[]): void {
    for (const stat of stats) {
      this.addToDataCache(stat.id, stat);
    }
  }

  addToDataCache(id: number, data: TeamStats): void {
    this.data.set(id, data);
  }

  setTeamNames(teamNames: Pair[]): void {
    this.teamNames = teamNames;
  }

  getTeamNames(): Pair[] {
    return this.teamNames;
  }

  getCachedIds(): number[] {
    return Array.from(this.data.keys());
  }

  getTeamData(id: number): TeamStats {
    return this.data.get(id);
  }
}
