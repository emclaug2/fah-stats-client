import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import { FahStatsComponent } from './fah-stats/fah-stats.component';
import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';
import {ChartistModule} from 'ng-chartist';
import {FormsModule} from '@angular/forms';
import {ChartComponent} from './fah-stats/chart/chart.component';
import {FooterComponent} from './fah-stats/footer/footer.component';
import { AboutComponent } from './fah-stats/about/about.component';
import {AppRoutingModule} from './app.routing';

@NgModule({
  declarations: [
    AppComponent,
    ChartComponent,
    FahStatsComponent,
    FooterComponent,
    AboutComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    ChartistModule,
    CoreModule,
    FormsModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
