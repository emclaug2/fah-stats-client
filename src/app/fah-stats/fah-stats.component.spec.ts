import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FahStatsComponent } from './fah-stats.component';

describe('FahStatsComponent', () => {
  let component: FahStatsComponent;
  let fixture: ComponentFixture<FahStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FahStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FahStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
