import {Component, Input, OnChanges} from '@angular/core';
import {ChartType} from 'ng-chartist';
import * as Chartist from 'chartist';
import {TeamStats} from '../../shared/model/TeamStats';
import {events, resetAnimationCounter} from './animation-events';
import {FahDataFormatService} from '../../core/fah-data-format/fah-data-format.service';
require('chartist-plugin-legend');

@Component({
  selector: 'dynamic-chart',
  template: `
    <x-chartist [data]="data"
                [type]="type"
                [options]="options"
                [events]="animationEvents"></x-chartist>
    <div *ngIf="teamStats" class="credits legend">credits * 10 million</div>
    <div *ngIf="teamStats" class="date legend">date</div>
  `,
  styleUrls: ['chart.component.scss']
})
export class ChartComponent implements OnChanges {

  @Input() teamStats: TeamStats[];

  type: ChartType;
  data: Chartist.IChartistData;
  chartTypes: ChartType[];
  legendNames: string[] = [];
  animationEvents: any;
  options: any;

  constructor(private fahDataFormatService: FahDataFormatService) {
    this.chartTypes = ['Line'];
    this.animationEvents = events;
    this.type = 'Line';
  }

  ngOnChanges(): void {
    if (!this.teamStats) {
      return;
    }
    const labels = this.populateLabels(this.teamStats);
    const series = this.populateSeries(this.teamStats);
    this.legendNames = this.populateLegendNames(this.teamStats);
    this.data = {labels, series};
    this.recreateGraphConfig();
  }

  private populateLegendNames(teamStats: TeamStats[]): string[] {
    const legend = [];
    for (const team of teamStats) {
      legend.push(team.name);
    }
    return legend;
  }

  private populateLabels(teamStats: TeamStats[]): string[] {
    const maxLabels = 5;
    const timestamps = this.fahDataFormatService.getAllTimestamps(teamStats);
    const interval = Math.round(timestamps.length / maxLabels);
    const labels = [];
    let count = 0;
    for (const timestamp of timestamps) {
      if (count++ === 0) {
        const date = new Date(timestamp);
        const timestring = Number(date.getMonth() + 1) + '/' +
          date.getDate() + '/' + date.getFullYear().toString().substr(2, 4);
        labels.push(timestring);
      } else {
        labels.push(undefined);
      }
      if (count === interval) {
        count = 0;
      }
    }
    return labels;
  }

  private populateSeries(teamStats: TeamStats[]): any[][] {
    const series = [];
    for (const stats of teamStats) {
      const teamSeries = [];
      const map = stats.date;
      for (const timestamp of Array.from(map.keys()).sort()) {
        teamSeries.push(map.get(timestamp).credit / 10000000);
      }
      series.push(teamSeries);
    }
    return series;
  }

  // Passes in new legend info, new configs on graph-reset.
  private recreateGraphConfig(): void {
    resetAnimationCounter();
    this.options = {
      height: '45vh',
      axisX: {
        showLabel: true
      },
      axisY: {
        onlyInteger: true,
        showLabel: true,
        offset: 60
      },
      fullWidth: true,
      chartPadding: {
        right: 40,
      },
      lineSmooth: Chartist.Interpolation.cardinal(),
      plugins: [
        Chartist.plugins.legend({
          legendNames: this.legendNames
        })
      ]
    };
  }
}
