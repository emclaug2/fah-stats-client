import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {routeNames} from '../../shared/model/routeNames';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  showHome(): void {
    this.router.navigateByUrl(routeNames.HOME);
  }
}
