import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FahApiService} from '../core/fah-api/fah-api.service';
import {TeamStats} from '../shared/model/TeamStats';
import {FormControl} from '@angular/forms';
import {Pair} from '../shared/dto/Pair';
import {MatCheckboxChange} from '@angular/material';
import {Router} from '@angular/router';
import {routeNames} from '../shared/model/routeNames';
import {CachedDataService} from '../core/cached-data/cached-data.service';
declare const $: any;
@Component({
  selector: 'app-fah-stats',
  templateUrl: 'fah-stats.component.html',
  styleUrls: ['fah-stats.component.scss']
})
export class FahStatsComponent implements OnInit {

  stats: TeamStats[];
  teamNamePairs: Pair[] = [];
  teamSelect = new FormControl();

  teamName: string;
  teamNumber: string;

  isLoading: boolean;
  isOrderedByName: boolean;
  isOrderedByRank = true;

  constructor(private apiService: FahApiService,
              private ref: ChangeDetectorRef,
              private cachedDataService: CachedDataService,
              private router: Router) {}

  ngOnInit(): void {
    if (this.cachedDataService.getTeamNames().length === 0) {
      this.populateDropdownData();
    } else {
      this.teamNamePairs = this.cachedDataService.getTeamNames();
    }
  }

  populateDropdownData(): void {
    this.apiService.getTop100Pairs()
      .then((pairs: Pair[]) => {
        this.teamNamePairs = pairs;
        this.cachedDataService.setTeamNames(pairs);
        this.orderTeamsByRank();
      }).catch(err => {
        console.error(err);
    });
  }

  orderTeamsByRank(): void {
    this.teamNamePairs.sort((a, b) => (a.rank > b.rank) ? 1 : -1);
  }

  orderTeamsByName(): void {
    this.teamNamePairs.sort((a, b) => (a.name.toUpperCase() > b.name.toUpperCase()) ? 1 : -1);
  }

  onRankCheckboxChange(e: MatCheckboxChange): void {
    this.isOrderedByRank = e.checked;
    this.isOrderedByName = !e.checked;
    this.adjustTeamOrder();
  }

  onNameCheckboxChange(e: MatCheckboxChange): void {
    this.isOrderedByName = e.checked;
    this.isOrderedByRank = !e.checked;
    this.adjustTeamOrder();
  }

  adjustTeamOrder(): void {
    if (this.isOrderedByName) {
      this.orderTeamsByName();
    } else if (this.isOrderedByRank) {
      this.orderTeamsByRank();
    }
  }

  search(): void {
    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.ref.detectChanges();
    const cacheResult = this.getCachedData();
    if (cacheResult.nonCachedIds.length === 0) {
      this.stats = cacheResult.cachedData;
      this.isLoading = false;
    } else {
      this.getDataViaApi(cacheResult);
    }
  }

  getDataViaApi(cachedResult: CachedResult): void {
    this.apiService.getTeamStats(cachedResult.nonCachedIds)
      .then(data => {
        this.cachedDataService.cacheTeamData(data);
        this.stats = data.concat(cachedResult.cachedData);
        this.isLoading = false;
      }).catch(err => {
        console.error(err);
        this.isLoading = false;
    });
  }

  isNoTeamSelected(): boolean {
    if (!this.teamSelect || !this.teamSelect.value) {
      return true;
    }
    return this.teamSelect.value.length === 0;
  }

  getCachedData(): CachedResult {
    const selectedIds: number[] = this.teamSelect.value;
    const cachedData: TeamStats[] = [];
    const nonCachedIds: number[] = [];
    for (const selectedId of selectedIds) {
      const data = this.cachedDataService.getTeamData(selectedId);
      data ? cachedData.push(data) : nonCachedIds.push(selectedId);
    }
    return new CachedResult(nonCachedIds, cachedData);
  }

  showAbout(): void {
    this.router.navigateByUrl(routeNames.ABOUT);
  }
}

// After the user looks up the data for the first time, we cache it so they can't re-search.
class CachedResult {
  constructor(public nonCachedIds: number[], public cachedData: TeamStats[]) {}
}

