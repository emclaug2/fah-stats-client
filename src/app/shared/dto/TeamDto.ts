export class TeamDto {
  constructor(public id: number,
              public name: string,
              public date: {}) {
  }
}
