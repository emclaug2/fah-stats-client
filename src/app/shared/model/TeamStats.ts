import {Datapoint} from './Datapoint';

export class TeamStats {
  constructor(public id: number,
              public name: string,
              public date: Map<number, Datapoint>) {
  }
}
