import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {routeNames} from './shared/model/routeNames';
import {FahStatsComponent} from './fah-stats/fah-stats.component';
import {AboutComponent} from './fah-stats/about/about.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: routeNames.HOME, component: FahStatsComponent },
  { path: routeNames.ABOUT, component: AboutComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
