# Fah Stats Client
Folding at Home client which shows top 100 team ranking and contribution history.
## Tools
This project was made using Angular 7, implementing Material design.  
CI/CD & version control provided by Gitlab.  
The web app is hosted by Firebase.

## Links

## Contributors
This project was made by Evan McLaughlin.   
